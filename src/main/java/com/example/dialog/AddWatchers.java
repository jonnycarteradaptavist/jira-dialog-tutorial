package com.example.dialog;

import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.user.UserUtils;
import org.apache.commons.lang.StringUtils;
import com.atlassian.jira.web.action.issue.AbstractIssueSelectAction;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: Document this class / interface here
 *
 * @since v4.0
 */
public class AddWatchers extends AbstractIssueSelectAction
{
    private final WatcherManager watcherManager;
    private final BeanFactory beanFactory;
    private String[] watcherUserNames = new String[0];
    private List<String> validUsernames = new ArrayList<String>();

    public AddWatchers(final WatcherManager watcherManager, final BeanFactory beanFactory)
    {
        this.watcherManager = watcherManager;
        this.beanFactory = beanFactory;
    }


    @Override
    public String doDefault() throws Exception
    {
        return INPUT;
    }

    @Override
    protected void doValidation()
    {
        for (String username : watcherUserNames)
        {
            username = username.trim();

            if (UserUtils.userExists(username))
            {
                validUsernames.add(username);
            }
            else
            {
                addErrorMessage(beanFactory.getInstance(getLoggedInUser()).getText("tutorial.errors.user", username));
            }
        }
    }

    @Override
    public String doExecute() throws Exception
    {
        for (String validUsername : validUsernames)
        {
            watcherManager.startWatching(UserUtils.getUser(validUsername), getIssueObject());
        }

        return returnCompleteWithInlineRedirect("/browse/" + getIssueObject().getKey());
    }

    public void setWatcherUserNames(String userNames) throws Exception
    {
        if(StringUtils.isNotBlank(userNames))
        {
            watcherUserNames = userNames.split(",", 10); // split into array of user names

        }
    }
}
