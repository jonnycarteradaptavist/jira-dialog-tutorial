# Jira Dialog Tutorial

This is a tutorial that shows how to use dialogs.

## Running locally

To run this app locally, make sure that you have the Atlassian Plugin SDK installed, and then run:

* atlas-run   -- installs this plugin into Jira and starts it on http://localhost:2990/jira
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-cli   -- after atlas-run or atlas-debug, opens a Maven command line window:
                 - 'pi' reinstalls the plugin into the running Jira instance
* atlas-help  -- prints description for all commands in the SDK

Full documentation is always available at: [Set up the Atlassian Plugin SDK and build a project][1].

 [1]: https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project
